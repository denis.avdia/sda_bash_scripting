declare -A myArray
myArray["1"]="value1"
myArray["2"]="value2"
myArray["3"]="value3"

echo ${myArray["1"]}  # Output: value1
echo ${myArray["2"]}  # Output: value2
echo ${myArray["3"]}  # Output: value3

for key in "${!myArray[@]}"; do
    echo "Key: $key, Value: ${myArray[$key]}"
done